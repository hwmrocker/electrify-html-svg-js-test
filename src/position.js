
export class Position {
	constructor(x, y = null, absolute = true) {
		this.absolute = absolute;
		if (y === null) {
			if (x instanceof Position){
				this.x = x.x;
				this.y = x.y;
			}
		} else {
			this.x = x;
			this.y = y;
		}
	}

	add(x, y = null) {
		let _x;
		let _y;
		if (y === null) {
			if (x instanceof Position){
				_x = x.x;
				_y = x.y;
			}
		} else {
			_x = x;
			_y = y;
		}
		// return new Position(_x + this.x, _y + this.y, this.absolute);
		this.x += _x;
		this.y += _y;
	}

	nextPosition(value) {
		// https://www.w3.org/TR/SVG/paths.html#PathData
		// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d
		const rfind = /([vVhHlL]?)[ ]?([-]?\d+)[ ,]?([-]?\d+)?/g;
		let match = rfind.exec(value);
		const command = match[1];
		const x = parseInt(match[2], 10);
		const y = match[3] ? parseInt(match[3], 10) : undefined;
		match = undefined;
		if (command && command === command.toUpperCase()) {
			this.absolute = true;
		} else if (command && command === command.toUpperCase()) {
			this.absolute = false;
		}
		// console.log(` cmd="${command}" x${x} y${y}`);
		switch (command) {
			case 'v':
				this.y += x;
				break;
			case 'V':
				this.y = x;
				break;
			case 'h':
				this.x += x;
				break;
			case 'H':
				this.x = x;
				break;
			// case 'l':
			// case 'L':
			default:
				if (this.absolute) {
					this.x = x;
					this.y = y;
				} else {
					this.add(x, y);
				}
		}
		return this;
	}

	toString() {
		return `Position(${this.x}, ${this.y})`;
	}

}

