import { Position } from 'position';

const signalColor = 'rgb(219, 231, 32)';
const tickTimeout = 50;
const pixlesPerSecond = 300;
const distancePerTick = pixlesPerSecond / (1000 / tickTimeout);
const radius = 6;

class WireSignal {
	constructor(id, parent, width, forward = true) {
		this.id = id;
		this.parent = parent;
		this.width = width;
		this.forward = forward;
		this.tick = 0;
		this.output = false;

		// select path
		const path = this.path = document.querySelector(`#${this.id}`);

		// we need to add 2 pixles to the total lenght, so that the signal is not visble
		// after the animation is finished; 
		// For Firefox 1 pixel is enough, Chromium needs 2
		const length = this.length = path.getTotalLength() + 2;

		// Clear any previous transition
		path.style.transition = path.style.WebkitTransition = 'none';

		// Set up the starting positions
		path.style.strokeDasharray = `${width} ${length}`;
		path.style.strokeDashoffset = forward ? width : -length;

		// Trigger a layout so styles are calculated & the browser
		// picks up the starting position before animating
		path.getBoundingClientRect();

		path.style.transition = path.style.WebkitTransition = 'stroke-dashoffset 0.1s linear';
	}

	dotick() {
		const path = this.path;
		const length = this.length;
		this.tick += 1;
		const distance = this.tick * distancePerTick;

		// Trigger a layout so styles are calculated & the browser
		// picks up the starting position before animating
		path.getBoundingClientRect();

		const newPos = this.forward ? Math.max(this.width - distance, -length + 1) : Math.min(-length + distance, this.width + 1);

		// Set up the final position
		path.style.strokeDashoffset = newPos;

		if (distance >= (length + this.width + 1)){
			return { type: 'end', delta: distance - this.width - length - 1 - distancePerTick };
		}
		if (!this.output && distance >= (length + 1)){
			this.output = true;
			return { type: 'output', delta: distance - length - 1 - distancePerTick };
		}
	}
}

class Wire {
	constructor(pathId) {
		this.id = pathId;
		this.signals = { A: [], B: [] };
		this.output = { A: [], B: [] };
	}
	addSignal(width, port = 'A') {
		const forward = port === 'A';
		const pathId = this.id;
		const originalPath = document.querySelector(`#${this.id}`);

		// we need to change the id and the color of the new element and not the original one
		const clonedPath = originalPath.cloneNode(true);

		// construct the newId, TODO dont use random, use ticks as soon as they are present
		const newId = `${originalPath.getAttribute('id')}${Math.floor(Math.random() * 1000000)}`;
		clonedPath.setAttribute('id', newId);
		clonedPath.style.stroke = signalColor;

		// append the clonedPath after the originalPath
		originalPath.insertAdjacentHTML('afterend', clonedPath.outerHTML);

		this.signals[port].push(new WireSignal(newId, pathId, width, forward));
	}

	consumeOutputSignals(port) {
		return this.output[port].splice(0, this.output[port].length);
	}

	tick() {
		const that = this;
		['A', 'B'].forEach((port) => {
			const removals = [];
			that.signals[port].forEach((signal, index) => {
				const result = signal.dotick();
				if (result && result.type === 'output') {
					console.log(`signal ${signal.id} is leaving (${result.delta})`);
					that.output[port === 'A' ? 'B' : 'A'].push(signal);

				} else if (result && result.type === 'end') {
					console.log(`signal ${signal.id} has finished (${result.delta})`);
					const el = document.querySelector(`#${signal.id}`);
					el.parentNode.removeChild(el);
					removals.unshift(index);
				}
			});
			removals.forEach((index) => {
				that.signals[port].splice(index, 1);
			});
		});
	}

}


class Connection {
	constructor(wire, port) {
		this.wire = wire;
		this.port = port;
	}
}


class Connector {
	connections = {};

	addConnection(wire, port) {
		this.connections[wire.id] = new Connection(wire, port);
	}

	consumeInput() {
		// that this or even this that is unnötig
		// weil die arrow funktion kein eigenes this hat.
		const that = this;
		for (const key in this.connections) {
			if ({}.hasOwnProperty.call(this.connections, key)) {
				const connection = this.connections[key];
				connection.wire.consumeOutputSignals(connection.port).forEach((signal) => {
					that.provideOutput(key, signal);
				});
			}
		}
	}

	provideOutput(except, signal) {
		for (const key in this.connections) {
			if ({}.hasOwnProperty.call(this.connections, key)) {
				if (key !== except){
					const connection = this.connections[key];
					connection.wire.addSignal(signal.width, connection.port);
				}
			}

		}
	}
}

function getStartAndEnd(pathId, markBendings = false) {
	// returns start and end positions of a simple svg path
	const path = document.querySelector(`#${pathId}`);
	let startPosition;

	// we accept all paths that produce straight lines
	const regexChecker = /[mM]( [vVhH][ ]?[-]?\d+| [lL]?[-]?\d+[ ,][-]?\d+)+$/;
	const regexFinder = /( [vVhH][ ]?[-]?\d+| [lL]?[-]?\d+[ ,][-]?\d+)/g;
	const pathData = path.getAttribute('d');

	if (!regexChecker.test(pathData)) {
		throw new Error(`pathData is not valid or supported\n pathData = "${pathData}"\n test_regex = ${regexChecker}`);
	}

	const absolute = (pathData[0] === 'M');
	const iposition = new Position(0, 0, absolute);
	pathData.match(regexFinder).forEach((item, index) => {
		iposition.nextPosition(item);
		// console.log(item, `cx="${iposition.x}" cy="${iposition.y}"`);

		if (markBendings) {
			document.querySelector(`#${pathId}`).insertAdjacentHTML('beforebegin', `<circle cx="${iposition.x}" cy="${iposition.y}" r="${radius}"/>`);
		}

		if (index === 0) {
			startPosition = new Position(iposition);
		}
	});
	return {
		start: startPosition,
		end: iposition,
	};

}
// getStartAndEnd('#path2985');
// const wire = new Wire('path2985');
// const wire2 = new Wire('path2986');
// const connector = new Connector();

// connector.addConnection(wire, 'B');
// connector.addConnection(wire2, 'A');

// setTimeout(wire.addSignal.bind(wire), 0, 10, 'A');
// setTimeout(wire.addSignal.bind(wire), 900, 300, 'B');
// setTimeout(wire.addSignal.bind(wire), 1300, 50);
// setTimeout(wire.addSignal.bind(wire), 1700, 30);

// function ticker() {
// 	setTimeout(ticker, tickTimeout);
// 	wire.tick();
// 	wire2.tick();
// 	connector.consumeInput();

// }

// setTimeout(ticker, 0);


const paths = ['path01', 'path02', 'path03', 'path04', 'path05', 'path06', 'path07', 'path08', 'path09', 'path10',
 'path11', 'path12', 'path13', 'path14', 'path15', 'path16', 'path17', 'path18', 'path19', 'path20',
 'path21', 'path22', 'path23', 'path24', 'path25', 'path26',
];
const wires = {};
let conn;
const connectors = [];
paths.forEach((id) => {
	const wire = new Wire(id);
	wires[id] = wire;
	const se = getStartAndEnd(id);
	// console.log(`${se.end.x},${se.end.y}, wires['${id}'], 'B'`);
	// console.log(`${se.start.x},${se.start.y}, wires['${id}'], 'A'`);
});
// 140,150, wires.path06, 'B'
// 140,150, wires.path08, 'A'
// 140,150, wires.path09, 'A'
conn = new Connector();
conn.addConnection(wires.path06, 'B');
conn.addConnection(wires.path08, 'A');
conn.addConnection(wires.path09, 'A');

// 140,80, wires.path09, 'B'
// 140,80, wires.path10, 'A'
// 140,80, wires.path11, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path09, 'B');
conn.addConnection(wires.path10, 'A');
conn.addConnection(wires.path11, 'A');


// 210,130, wires.path12, 'A'
// 210,130, wires.path13, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path08, 'B');
conn.addConnection(wires.path12, 'A');
conn.addConnection(wires.path13, 'A');

// 290,10, wires.path14, 'A'
// 290,10, wires.path15, 'A'
// 290,10, wires.path16, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path12, 'B');
conn.addConnection(wires.path14, 'A');
conn.addConnection(wires.path15, 'A');
conn.addConnection(wires.path16, 'A');

// 335,10, wires.path15, 'B'
// 335,10, wires.path17, 'A'
// 335,10, wires.path18, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path15, 'B');
conn.addConnection(wires.path17, 'A');
conn.addConnection(wires.path18, 'A');

// 40,150, wires.path01, 'B'
// 40,150, wires.path02, 'A'
// 40,150, wires.path03, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path01, 'B');
conn.addConnection(wires.path02, 'A');
conn.addConnection(wires.path03, 'A');

// 40,80, wires.path03, 'B'
// 40,80, wires.path04, 'A'
// 40,80, wires.path05, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path03, 'B');
conn.addConnection(wires.path04, 'A');
conn.addConnection(wires.path05, 'A');

// 405,150, wires.path19, 'A'
// 405,150, wires.path20, 'A'
// 425,150, wires.path20, 'B'
// 425,150, wires.path21, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path18, 'B');
conn.addConnection(wires.path19, 'A');
conn.addConnection(wires.path20, 'A');

connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path20, 'B');
conn.addConnection(wires.path21, 'A');

// 425,80, wires.path21, 'B'
// 425,80, wires.path22, 'A'
// 425,80, wires.path23, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path21, 'B');
conn.addConnection(wires.path22, 'A');
conn.addConnection(wires.path23, 'A');

// 455,10, wires.path23, 'B'
// 455,10, wires.path24, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path23, 'B');
conn.addConnection(wires.path24, 'A');

// 480,80, wires.path25, 'A'
// 480,80, wires.path26, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path24, 'B');
conn.addConnection(wires.path25, 'A');
conn.addConnection(wires.path26, 'A');

// 90,150, wires.path02, 'B'
// 90,150, wires.path06, 'A'
// 90,150, wires.path07, 'A'
connectors.push(conn);
conn = new Connector();
conn.addConnection(wires.path02, 'B');
conn.addConnection(wires.path06, 'A');
conn.addConnection(wires.path07, 'A');

connectors.push(conn);
// const wire = new Wire('path2985');
// const wire2 = new Wire('path2986');
// const connector = new Connector();

// connector.addConnection(wire, 'B');
// connector.addConnection(wire2, 'A');

// setTimeout(wire.addSignal.bind(wire), 0, 10, 'A');
// setTimeout(wire.addSignal.bind(wire), 900, 300, 'B');
// setTimeout(wire.addSignal.bind(wire), 1300, 50);
// setTimeout(wire.addSignal.bind(wire), 1700, 30);

function ticker() {
	setTimeout(ticker, tickTimeout);
	for (const key in wires) {
		if ({}.hasOwnProperty.call(wires, key)) {
			wires[key].tick();
		}
	}
	connectors.forEach((conn) => {
		conn.consumeInput();
	});
	// wire.tick();
	// wire2.tick();
	// connector.consumeInput();

}

setTimeout(ticker, 0);


function generator() {
	console.log('woosh');
	setTimeout(generator, 5000);
	wires.path01.addSignal(200, 'A');
}

setTimeout(generator, 500);
