var production = process.env.NODE_ENV === "production";
var path = require("path");
var webpack = require("webpack");

module.exports = {
	devtool: production ? undefined : "cheap-module-source-map",
	eslint: {
		configFile: "./.eslintrc.yml",
		failOnWarnings: false,
		failOnError: true

	},
	context: __dirname,
	resolve: {
		root: path.resolve('src')
	},
	entry: {
		index: './src/index.js'
	},
	output: {
		path: path.resolve("static"),
		filename: 'main.js',
		publicPath: ''
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel!eslint'
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			__DEV__: JSON.stringify(!production),
			__PROD__: JSON.stringify(production)
		})
	]


}